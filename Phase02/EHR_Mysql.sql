-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: ehr
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ehr_disease`
--

DROP TABLE IF EXISTS `ehr_disease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_disease` (
  `english_name` varchar(255) NOT NULL,
  `persian_name` varchar(255) DEFAULT NULL,
  `descript` varchar(1000) NOT NULL,
  PRIMARY KEY (`english_name`),
  UNIQUE KEY `english_name` (`english_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_disease`
--

LOCK TABLES `ehr_disease` WRITE;
/*!40000 ALTER TABLE `ehr_disease` DISABLE KEYS */;
INSERT INTO `ehr_disease` VALUES ('Abdominal','','An abdominal aortic aneurysm (AAA) is a swelling (aneurysm) of the aorta – the main blood vessel that leads away from the heart, down through the abdomen to the rest of the body.'),('Abdominal aortic aneurysm','','An abdominal aortic aneurysm (AAA) is a swelling (aneurysm) of the aorta – the main blood vessel that leads away from the heart, down through the abdomen to the rest of the body.'),('Acne','','Acne is a common skin condition that affects most people at some point. It causes spots, oily skin and sometimes skin that\'s hot or painful to touch.'),('Acute cholecystitis','','Acute cholecystitis is swelling (inflammation) of the gallbladder. It is a potentially serious condition that usually needs to be treated in hospital.'),('Acute lymphoblastic leukaemia','','Leukaemia is cancer of the white blood cells. Acute leukaemia means it progresses rapidly and aggressively, and usually requires immediate treatment.'),('Anaphylaxis','','Anaphylaxis is a severe, potentially life-threatening allergic reaction that can develop rapidly.'),('Angioedema','','Angioedema is the swelling of the deeper layers of the skin, caused by a build-up of fluid.'),('Anorexia nervosa','','Anorexia nervosa is an eating disorder and a serious, potentially life-threatening, mental health condition.'),('leukaemia','','This section is for teenagers and young adults and is about a type of cancer called acute lymphoblastic leukaemia (ALL). The other main type of leukaemia that can affect teenagers and young adults is acute myeloid leukaemia.'),('myeloid leukaemia','','Leukaemia is cancer of the white blood cells. Acute leukaemia means it progresses rapidly and aggressively, and usually requires immediate treatment.');
/*!40000 ALTER TABLE `ehr_disease` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_doctor`
--

DROP TABLE IF EXISTS `ehr_doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_doctor` (
  `doctor_id` int NOT NULL,
  `personal_digit` varchar(12) NOT NULL,
  `do_degree` varchar(255) NOT NULL,
  `major` varchar(255) NOT NULL,
  `graduated_from` varchar(255) NOT NULL,
  `number_of_visit` int DEFAULT NULL,
  `salary` text NOT NULL,
  PRIMARY KEY (`doctor_id`),
  UNIQUE KEY `doctor_id` (`doctor_id`),
  UNIQUE KEY `personal_digit` (`personal_digit`),
  CONSTRAINT `ehr_doctor_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `ehr_person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_doctor`
--

LOCK TABLES `ehr_doctor` WRITE;
/*!40000 ALTER TABLE `ehr_doctor` DISABLE KEYS */;
INSERT INTO `ehr_doctor` VALUES (1,'965412134587','PhD','Allergists','University of Tehran',54,'12000000'),(2,'975487134587','PhD','Allergists','University of Tehran',12,'12000000'),(3,'925423114587','PhD','Dermatologists','University of Tehran',524,'12000000'),(4,'955117234587','PhD','Ophthalmologists','University of Tehran',514,'12000000'),(5,'931117134587','PhD','Obstetrician','University of Tehran',514,'12000000'),(6,'965482224587','PhD','Cardiologists','Shahid Beheshti University',154,'12000000'),(7,'915487133337','PhD','Obstetrician','University of Tehran',243,'12000000'),(8,'955487124587','PhD','Dentist','University of Iran',321,'12000000'),(9,'975443134587','PhD','Cardiologists','Shahid Beheshti University',114,'12000000');
/*!40000 ALTER TABLE `ehr_doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_drug`
--

DROP TABLE IF EXISTS `ehr_drug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_drug` (
  `english_name` varchar(255) NOT NULL,
  `persian_name` varchar(255) DEFAULT NULL,
  `descript` varchar(1000) NOT NULL,
  PRIMARY KEY (`english_name`),
  UNIQUE KEY `english_name` (`english_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_drug`
--

LOCK TABLES `ehr_drug` WRITE;
/*!40000 ALTER TABLE `ehr_drug` DISABLE KEYS */;
INSERT INTO `ehr_drug` VALUES ('Amitriptyline','','Amitriptyline is a tricyclic antidepressant with sedative effects. Amitriptyline affects certain chemical messengers (neurotransmitters) that communicate between brain cells and help regulate mood.'),('Amlodipine','','Amlodipine is a calcium channel blocker that dilates (widens) blood vessels and improves blood flow.'),('Amoxicillin','','Amoxicillin is a penicillin antibiotic that fights bacteria.'),('Ativan','','Ativan (lorazepam) belongs to a group of drugs called benzodiazepines. It is thought that lorazepam works by enhancing the activity of certain neurotransmitters in the brain.'),('Atorvastatin','','Atorvastatin belongs to a group of drugs called HMG CoA reductase inhibitors, or statins.'),('Azithromycin','','Azithromycin is an antibiotic that fights bacteria.'),('Benzonatate','','Benzonatate is a non-narcotic cough medicine.'),('Brilinta','','Brilinta (ticagrelor) prevents platelets in your blood from sticking together to form an unwanted blood clot that could block an artery.'),('Lisinopril','','Lisinopril is an ACE inhibitor. ACE stands for angiotensin converting enzyme.'),('Loratadine','','Loratadine is an antihistamine that reduces the effects of natural chemical histamine in the body. Histamine can produce symptoms of sneezing, itching, watery eyes, and runny nose.');
/*!40000 ALTER TABLE `ehr_drug` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_drug_prescription`
--

DROP TABLE IF EXISTS `ehr_drug_prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_drug_prescription` (
  `dp_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `drug_id` varchar(255) NOT NULL,
  `prescription_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`dp_id`),
  UNIQUE KEY `dp_id` (`dp_id`),
  KEY `drug_id` (`drug_id`),
  KEY `prescription_id` (`prescription_id`),
  CONSTRAINT `ehr_drug_prescription_ibfk_1` FOREIGN KEY (`drug_id`) REFERENCES `ehr_drug` (`english_name`),
  CONSTRAINT `ehr_drug_prescription_ibfk_2` FOREIGN KEY (`prescription_id`) REFERENCES `ehr_prescription` (`prescription_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_drug_prescription`
--

LOCK TABLES `ehr_drug_prescription` WRITE;
/*!40000 ALTER TABLE `ehr_drug_prescription` DISABLE KEYS */;
INSERT INTO `ehr_drug_prescription` VALUES (1,'Amitriptyline',1),(2,'Lisinopril',1),(3,'Amitriptyline',2),(4,'Ativan',3),(5,'Amitriptyline',4),(6,'Lisinopril',5),(7,'Amitriptyline',5),(8,'Azithromycin',6),(9,'Lisinopril',7),(10,'Azithromycin',8);
/*!40000 ALTER TABLE `ehr_drug_prescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_drug_store`
--

DROP TABLE IF EXISTS `ehr_drug_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_drug_store` (
  `drug_store_id` int NOT NULL,
  `ds_name` varchar(255) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `establishment_date` date NOT NULL,
  `fk_head` int NOT NULL,
  PRIMARY KEY (`drug_store_id`),
  UNIQUE KEY `drug_store_id` (`drug_store_id`),
  KEY `fk_head` (`fk_head`),
  CONSTRAINT `ehr_drug_store_ibfk_1` FOREIGN KEY (`fk_head`) REFERENCES `ehr_person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_drug_store`
--

LOCK TABLES `ehr_drug_store` WRITE;
/*!40000 ALTER TABLE `ehr_drug_store` DISABLE KEYS */;
INSERT INTO `ehr_drug_store` VALUES (1,'Ali Panahi Drug Store','Iran, Tehran','72716716','1234567891','1399-03-21',2),(2,'Dr.Rezaei Drug Store','Iran, Isfahan','72746276','1234567891','1398-04-12',3),(3,'Drug Store 01','Iran, Qom','127542716','12456891','1396-02-28',2),(4,'Drug Store 02','Iran, Tabriz','143562716','134567891','1399-03-11',6),(5,'Drug Store 03','Iran, Tehran','4274625326','13567891','1396-04-21',2),(6,'Drug Store 04','Iran, Birjand','227232716','123567891','1395-05-16',3),(7,'Drug Store 05','Iran, Mashhad','121462123','123567891','1392-03-18',2),(8,'Drug Store 06','Iran, Shiraz','327543516','123467891','1393-06-12',2),(9,'Drug Store 07','Iran, Tabriz','127562716','123467891','1394-07-02',8),(10,'Drug Store 08','Iran, Tabriz','124362716','123467891','1395-12-11',2);
/*!40000 ALTER TABLE `ehr_drug_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_health_centre`
--

DROP TABLE IF EXISTS `ehr_health_centre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_health_centre` (
  `centre_id` int NOT NULL,
  `hc_name` varchar(255) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `private` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `establishment_date` date NOT NULL,
  `fk_head` int NOT NULL,
  PRIMARY KEY (`centre_id`),
  UNIQUE KEY `centre_id` (`centre_id`),
  KEY `fk_head` (`fk_head`),
  CONSTRAINT `ehr_health_centre_ibfk_1` FOREIGN KEY (`fk_head`) REFERENCES `ehr_person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_health_centre`
--

LOCK TABLES `ehr_health_centre` WRITE;
/*!40000 ALTER TABLE `ehr_health_centre` DISABLE KEYS */;
INSERT INTO `ehr_health_centre` VALUES (1,'Shahid Beheshti','Irean , Tehran','1234567891',1,'41526359874','1366-03-21',1),(2,'Shahid Beheshti','Irean , Tehran','1234567891',1,'41526359874','1366-12-11',1),(3,'Tehran','Irean , Tehran','1234567891',1,'43526359874','1376-03-21',2),(4,'Masih Daneshavri','Irean , Tehran','1234567891',1,'41526359874','1386-11-23',2),(5,'Iran','Irean , Tehran','1234567891',1,'41526326824','1396-09-24',1),(6,'Health Centre 01','Irean , Tehran','1234567891',1,'41526359874','1336-05-12',1),(7,'Health Centre 02','Irean , Tehran','1234567891',1,'35926359874','1346-02-25',3),(8,'Health Centre 03','Irean , Tehran','1234567891',1,'41526359874','1356-04-21',3),(9,'Health Centre 04','Irean , Tehran','1234567891',1,'98535959874','1366-01-21',3),(10,'Health Centre 05','Irean , Tehran','1234567891',1,'41524159414','1366-03-21',1);
/*!40000 ALTER TABLE `ehr_health_centre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_person`
--

DROP TABLE IF EXISTS `ehr_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_person` (
  `person_id` int NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `age` int NOT NULL,
  `birth_day` date NOT NULL,
  `alive` tinyint(1) NOT NULL,
  `birth_place` varchar(255) NOT NULL,
  `phone_number` varchar(11) NOT NULL,
  `mobile_phone_number` varchar(11) NOT NULL,
  `allergies` varchar(1000) DEFAULT NULL,
  `residence_address` varchar(1000) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `mother` int DEFAULT NULL,
  `father` int DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  UNIQUE KEY `person_id` (`person_id`),
  KEY `mother` (`mother`),
  KEY `father` (`father`),
  CONSTRAINT `ehr_person_ibfk_1` FOREIGN KEY (`mother`) REFERENCES `ehr_person` (`person_id`),
  CONSTRAINT `ehr_person_ibfk_2` FOREIGN KEY (`father`) REFERENCES `ehr_person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_person`
--

LOCK TABLES `ehr_person` WRITE;
/*!40000 ALTER TABLE `ehr_person` DISABLE KEYS */;
INSERT INTO `ehr_person` VALUES (1,'Reza','Akbari',23,'1356-03-04',1,'Tehran','64851794','19159462693',NULL,'Iran, Tehran','male',NULL,NULL),(2,'Ali','Sohrabi',33,'1361-03-04',1,'Isfahan','64851794','19159462693',NULL,'Iran, Isfahan','male',NULL,NULL),(3,'Akbar','Rezaie',43,'1384-03-04',1,'Shiraz','64851794','19159462693',NULL,'Iran, Shiraz','male',NULL,NULL),(4,'Sohrab','Mahmodi',53,'1379-03-04',1,'Tehran','64851794','19159462693',NULL,'Iran, Tehran','male',NULL,NULL),(5,'Jimmy','Abbasi',13,'1369-03-04',1,'Mashad','64851794','19159462693',NULL,'Iran, Mashad','male',NULL,NULL),(6,'Mahmod','Ahmadi',21,'1364-03-04',1,'Tehran','64851794','19159462693',NULL,'Iran, Tehran','male',NULL,NULL),(7,'Abbas','Akbari',29,'1362-03-04',1,'Qom','64851794','19159462693',NULL,'Iran, Qom','male',NULL,NULL),(8,'Zahra','Sohrabi',24,'1396-03-04',1,'Tehran','64851794','19159462693',NULL,'Iran, Tehran','female',NULL,NULL),(9,'Zeinab','Ahmadi',45,'1386-03-04',1,'Tabriz','64851794','19159462693',NULL,'Iran, Tabriz','female',NULL,NULL),(10,'Sakine','Namazi',94,'1376-03-04',1,'Tehran','64851794','19159462693',NULL,'Iran, Tehran','female',NULL,NULL);
/*!40000 ALTER TABLE `ehr_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_prescription`
--

DROP TABLE IF EXISTS `ehr_prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_prescription` (
  `prescription_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `permanent` tinyint(1) NOT NULL,
  `medicine_bought` tinyint(1) NOT NULL,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL,
  `visit_id` bigint unsigned NOT NULL,
  `drug_store_id` int NOT NULL,
  PRIMARY KEY (`prescription_id`),
  UNIQUE KEY `prescription_id` (`prescription_id`),
  KEY `visit_id` (`visit_id`),
  KEY `drug_store_id` (`drug_store_id`),
  CONSTRAINT `ehr_prescription_ibfk_1` FOREIGN KEY (`visit_id`) REFERENCES `ehr_visit` (`visit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ehr_prescription_ibfk_2` FOREIGN KEY (`drug_store_id`) REFERENCES `ehr_drug_store` (`drug_store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_prescription`
--

LOCK TABLES `ehr_prescription` WRITE;
/*!40000 ALTER TABLE `ehr_prescription` DISABLE KEYS */;
INSERT INTO `ehr_prescription` VALUES (1,0,1,'1399-03-11','1399-04-21',1,1),(2,0,1,'1399-04-12','1399-05-21',2,2),(3,1,1,'1399-05-13','1399-06-21',3,3),(4,1,0,'1399-06-14','1399-07-21',4,4),(5,1,0,'1399-07-15','1399-08-21',5,1),(6,0,0,'1399-08-16','1399-09-21',6,3),(7,1,0,'1399-09-17','1399-10-21',7,5),(8,1,1,'1399-03-18','1399-06-21',8,1),(9,0,1,'1399-03-19','1399-06-21',9,2),(10,1,1,'1399-03-21','1399-06-21',10,5);
/*!40000 ALTER TABLE `ehr_prescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_surgery`
--

DROP TABLE IF EXISTS `ehr_surgery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_surgery` (
  `surgery_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `su_name` varchar(255) NOT NULL,
  `english_name` varchar(255) NOT NULL,
  `su_date` date NOT NULL,
  `surgery_type` varchar(255) NOT NULL,
  `descript` varchar(1000) NOT NULL,
  `su_comment` varchar(1000) NOT NULL,
  `su_time` time NOT NULL,
  `person_id` int NOT NULL,
  `doctor_id` int NOT NULL,
  `health_centre_id` int NOT NULL,
  PRIMARY KEY (`surgery_id`),
  UNIQUE KEY `surgery_id` (`surgery_id`),
  KEY `person_id` (`person_id`),
  KEY `health_centre_id` (`health_centre_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `ehr_surgery_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ehr_person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ehr_surgery_ibfk_2` FOREIGN KEY (`health_centre_id`) REFERENCES `ehr_health_centre` (`centre_id`),
  CONSTRAINT `ehr_surgery_ibfk_3` FOREIGN KEY (`doctor_id`) REFERENCES `ehr_doctor` (`doctor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_surgery`
--

LOCK TABLES `ehr_surgery` WRITE;
/*!40000 ALTER TABLE `ehr_surgery` DISABLE KEYS */;
INSERT INTO `ehr_surgery` VALUES (1,'Surgery 01','Surgery 01 English Name','1399-01-01','Brain Surgery 01','Description ...','Some comment text','04:01:34',1,2,2),(2,'Surgery 02','Surgery 02 English Name','1398-02-21','Brain Surgery 02','Description ...','Some comment text','09:21:34',2,3,3),(3,'Surgery 03','Surgery 03 English Name','1397-03-12','Brain Surgery 03','Description ...','Some comment text','14:31:34',3,4,4),(4,'Surgery 04','Surgery 04 English Name','1396-04-13','Brain Surgery 04','Description ...','Some comment text','15:44:34',4,1,5),(5,'Surgery 05','Surgery 05 English Name','1395-05-14','Brain Surgery 05','Description ...','Some comment text','16:45:34',5,1,6),(6,'Surgery 06','Surgery 06 English Name','1394-06-15','Brain Surgery 06','Description ...','Some comment text','17:12:34',6,7,7),(7,'Surgery 07','Surgery 07 English Name','1393-07-16','Brain Surgery 07','Description ...','Some comment text','18:14:34',7,8,8),(8,'Surgery 08','Surgery 08 English Name','1396-08-17','Brain Surgery 08','Description ...','Some comment text','19:24:34',8,6,1),(9,'Surgery 09','Surgery 09 English Name','1395-09-18','Brain Surgery 09','Description ...','Some comment text','20:31:34',8,4,3),(10,'Surgery 10','Surgery 10 English Name','1397-10-19','Brain Surgery 10','Description ...','Some comment text','14:51:34',9,3,4);
/*!40000 ALTER TABLE `ehr_surgery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_test`
--

DROP TABLE IF EXISTS `ehr_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_test` (
  `test_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `te_time` time NOT NULL,
  `te_date` date NOT NULL,
  `test_type` varchar(255) NOT NULL,
  `te_result` varchar(255) NOT NULL,
  `te_comment` varchar(1000) NOT NULL,
  `person_id` int NOT NULL,
  `health_centre_id` int NOT NULL,
  `doctor_id` int NOT NULL,
  PRIMARY KEY (`test_id`),
  UNIQUE KEY `test_id` (`test_id`),
  KEY `person_id` (`person_id`),
  KEY `doctor_id` (`doctor_id`),
  KEY `health_centre_id` (`health_centre_id`),
  CONSTRAINT `ehr_test_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ehr_person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ehr_test_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `ehr_doctor` (`doctor_id`),
  CONSTRAINT `ehr_test_ibfk_3` FOREIGN KEY (`health_centre_id`) REFERENCES `ehr_health_centre` (`centre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_test`
--

LOCK TABLES `ehr_test` WRITE;
/*!40000 ALTER TABLE `ehr_test` DISABLE KEYS */;
INSERT INTO `ehr_test` VALUES (1,'14:01:34','1399-03-11','Blood Test','O+','Some comment text',1,1,2),(2,'15:11:34','1398-04-11','Blood Test','O+','Some comment text',3,1,2),(3,'16:21:34','1397-05-11','Blood Test','O+','Some comment text',4,1,2),(4,'17:31:34','1396-06-11','Blood Test','O+','Some comment text',5,1,2),(5,'18:41:34','1395-07-11','Some other kind of Test01','Test result01','Some comment text',4,2,3),(6,'19:51:34','1395-08-11','Some other kind of Test02','Test result02','Some comment text',5,2,3),(7,'20:21:34','1398-09-11','Some other kind of Test03','Test result03','Some comment text',6,2,3),(8,'21:24:34','1391-10-11','Blood Test','AB','Some comment text',7,3,2),(9,'22:37:34','1388-11-11','Blood Test','O-','Some comment text',8,4,5),(10,'23:27:34','1387-12-11','Blood Test','Test result04','Some comment text',9,5,6);
/*!40000 ALTER TABLE `ehr_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_visit`
--

DROP TABLE IF EXISTS `ehr_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_visit` (
  `visit_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `vi_date` date NOT NULL,
  `permanent_disease` tinyint(1) NOT NULL,
  `vi_time` time NOT NULL,
  `person_id` int NOT NULL,
  `health_centre_id` int NOT NULL,
  `disease_id` varchar(255) NOT NULL,
  `doctor_id` int NOT NULL,
  PRIMARY KEY (`visit_id`),
  UNIQUE KEY `visit_id` (`visit_id`),
  KEY `person_id` (`person_id`),
  KEY `doctor_id` (`doctor_id`),
  KEY `health_centre_id` (`health_centre_id`),
  KEY `disease_id` (`disease_id`),
  CONSTRAINT `ehr_visit_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ehr_person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ehr_visit_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `ehr_doctor` (`doctor_id`),
  CONSTRAINT `ehr_visit_ibfk_3` FOREIGN KEY (`health_centre_id`) REFERENCES `ehr_health_centre` (`centre_id`),
  CONSTRAINT `ehr_visit_ibfk_4` FOREIGN KEY (`disease_id`) REFERENCES `ehr_disease` (`english_name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_visit`
--

LOCK TABLES `ehr_visit` WRITE;
/*!40000 ALTER TABLE `ehr_visit` DISABLE KEYS */;
INSERT INTO `ehr_visit` VALUES (1,'1399-03-21',0,'23:31:34',1,1,'Abdominal',2),(2,'1398-04-12',0,'08:31:34',1,1,'Abdominal',3),(3,'1396-02-28',0,'09:12:14',2,2,'Abdominal',4),(4,'1399-03-11',1,'10:23:24',3,3,'Abdominal',6),(5,'1396-04-21',0,'12:34:34',4,4,'Abdominal',2),(6,'1395-05-16',1,'11:45:14',5,5,'Abdominal',7),(7,'1392-03-18',0,'14:56:44',6,1,'Abdominal',2),(8,'1393-06-12',1,'16:11:54',7,2,'Abdominal',5),(9,'1394-07-02',0,'17:32:54',1,6,'Abdominal',8),(10,'1395-12-11',1,'18:51:34',3,7,'Abdominal',2);
/*!40000 ALTER TABLE `ehr_visit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehr_workplace`
--

DROP TABLE IF EXISTS `ehr_workplace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ehr_workplace` (
  `w_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `doctor_id` int NOT NULL,
  `health_centre_id` int NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  PRIMARY KEY (`w_id`),
  UNIQUE KEY `w_id` (`w_id`),
  KEY `health_centre_id` (`health_centre_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `ehr_workplace_ibfk_1` FOREIGN KEY (`health_centre_id`) REFERENCES `ehr_health_centre` (`centre_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ehr_workplace_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `ehr_doctor` (`doctor_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehr_workplace`
--

LOCK TABLES `ehr_workplace` WRITE;
/*!40000 ALTER TABLE `ehr_workplace` DISABLE KEYS */;
INSERT INTO `ehr_workplace` VALUES (1,1,1,'1399-01-01','1399-09-01'),(2,2,2,'1398-02-01',NULL),(3,3,3,'1397-03-01',NULL),(4,4,1,'1396-04-01','1398-10-01'),(5,5,4,'1395-05-01','1398-11-01'),(6,6,6,'1394-06-01','1397-12-01'),(7,1,6,'1393-07-01','1398-12-01'),(8,2,3,'1392-08-01','1398-12-01'),(9,7,8,'1391-09-01','1398-12-01'),(10,9,9,'1390-10-01','1398-12-01');
/*!40000 ALTER TABLE `ehr_workplace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'ehr'
--

--
-- Dumping routines for database 'ehr'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-10  0:56:04
