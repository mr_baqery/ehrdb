CREATE TABLE ehr_person (
  person_id           INT PRIMARY KEY UNIQUE NOT NULL,
  first_name          VARCHAR(255)           NOT NULL,
  last_name           VARCHAR(255)           NOT NULL,
  age                 INT                    NOT NULL,
  birth_day           DATE                   NOT NULL,
  alive               BOOLEAN                NOT NULL,
  birth_place         VARCHAR(255)           NOT NULL,
  phone_number        VARCHAR(11)            NOT NULL,
  mobile_phone_number VARCHAR(11)            NOT NULL,
  allergies           VARCHAR(1000),
  residence_address   VARCHAR(1000)          NOT NULL,
  gender              VARCHAR(100)           NOT NULL,
  mother              INT,
  father              INT,
  FOREIGN KEY (mother) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (father) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- Insert data
INSERT INTO `ehr`.`ehr_person` (`person_id`, `first_name`, `last_name`, `age`, `birth_day`, `alive`, `birth_place`, `phone_number`, `mobile_phone_number`, `residence_address`, `gender`)
VALUES ('1', 'Reza', 'Akbari', '23', '1356-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'male'),
('2', 'Ali', 'Sohrabi', '33', '1361-03-04', '1', 'Isfahan', '64851794', '19159462693', 'Iran, Isfahan', 'male'),
('3', 'Akbar', 'Rezaie', '43', '1384-03-04', '1', 'Shiraz', '64851794', '19159462693', 'Iran, Shiraz', 'male'),
('4', 'Sohrab', 'Mahmodi', '53', '1379-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'male'),
('5', 'Jimmy', 'Abbasi', '13', '1369-03-04', '1', 'Mashad', '64851794', '19159462693', 'Iran, Mashad', 'male'),
('6', 'Mahmod', 'Ahmadi', '21', '1364-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'male'),
('7', 'Abbas', 'Akbari', '29', '1362-03-04', '1', 'Qom', '64851794', '19159462693', 'Iran, Qom', 'male'),
('8', 'Zahra', 'Sohrabi', '24', '1396-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'female'),
('9', 'Zeinab', 'Ahmadi', '45', '1386-03-04', '1', 'Tabriz', '64851794', '19159462693', 'Iran, Tabriz', 'female'),
('10', 'Sakine', 'Namazi', '94', '1376-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'female');


CREATE TABLE ehr_health_centre (
  centre_id          INT PRIMARY KEY NOT NULL  UNIQUE,
  hc_name            VARCHAR(255)    NOT NULL,
  address            VARCHAR(1000)   NOT NULL,
  zip_code           VARCHAR(10)             NOT NULL,
  private            BOOLEAN         NOT NULL,
  phone              VARCHAR(11)             NOT NULL,
  establishment_date DATE            NOT NULL,
  fk_head            INT             NOT NULL,
  FOREIGN KEY (fk_head) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

INSERT INTO `ehr`.`ehr_health_centre` (`centre_id`, `hc_name`, `address`, `zip_code`, `private`, `phone`, `establishment_date`, `fk_head`)
VALUES ('1', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41526359874', '1366-03-21', '1'),
('2', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41526359874', '1366-12-11', '1'),
('3', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '43526359874', '1376-03-21', '2'),
('4', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41526359874', '1386-11-23', '2'),
('5', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41526326824', '1396-09-24', '1'),
('6', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41526359874', '1336-05-12', '1'),
('7', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '35926359874', '1346-02-25', '3'),
('8', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41526359874', '1356-04-21', '3'),
('9', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '98535959874', '1366-01-21', '3'),
('10', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41524159414', '1366-03-21','1'),



CREATE TABLE ehr_doctor (
  doctor_id       INT PRIMARY KEY UNIQUE          NOT NULL,
  personal_digit  INT UNIQUE                      NOT NULL,
  do_degree       VARCHAR(255)                    NOT NULL,
  major           VARCHAR(255)                    NOT NULL,
  graduated_from  VARCHAR(255)                    NOT NULL,
  number_of_visit INT,
  salary          INT                             NOT NULL,
  FOREIGN KEY (doctor_id) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);


INSERT INTO `ehr`.`ehr_doctor` (`doctor_id`, `personal_digit`, `do_degree`, `major`, `graduated_from`, `number_of_visit`, `salary`)
VALUES ('1', '965412134587', 'PhD', 'Allergists' ,'University of Tehran', 54 , '12000000'),
('2', '975487134587', 'PhD', 'Allergists' ,'University of Tehran', 12 , '12000000'),
('3', '925423114587', 'PhD', 'Dermatologists' ,'University of Tehran', 524 , '12000000'),
('4', '955117234587', 'PhD', 'Ophthalmologists' ,'University of Tehran', 514 , '12000000'),
('5', '931117134587', 'PhD', 'Obstetrician' ,'University of Tehran', 514 , '12000000'),
('6', '965482224587', 'PhD', 'Cardiologists' ,'Shahid Beheshti University', 154 , '12000000'),
('7', '915487133337', 'PhD', 'Obstetrician' ,'University of Tehran', 243 , '12000000'),
('8', '955487124587', 'PhD', 'Dentist' ,'University of Iran', 321 , '12000000'),
('9', '975443134587', 'PhD', 'Cardiologists' ,'Shahid Beheshti University', 114 , '12000000');

CREATE TABLE ehr_disease (
  english_name VARCHAR(255) UNIQUE PRIMARY KEY NOT NULL,
  persian_name VARCHAR(255)                    ,
  descript  VARCHAR(1000)                   NOT NULL
);

INSERT INTO `ehr`.`ehr_disease` (`english_name`, `persian_name`, `descript`)
VALUES ('Abdominal aortic aneurysm', '', "An abdominal aortic aneurysm (AAA) is a swelling (aneurysm) of the aorta – the main blood vessel that leads away from the heart, down through the abdomen to the rest of the body."),
('Acne', '', "Acne is a common skin condition that affects most people at some point. It causes spots, oily skin and sometimes skin that's hot or painful to touch."),
('Acute cholecystitis', '', "Acute cholecystitis is swelling (inflammation) of the gallbladder. It is a potentially serious condition that usually needs to be treated in hospital."),
('leukaemia', '', "This section is for teenagers and young adults and is about a type of cancer called acute lymphoblastic leukaemia (ALL). The other main type of leukaemia that can affect teenagers and young adults is acute myeloid leukaemia."),
('Acute lymphoblastic leukaemia', '', "Leukaemia is cancer of the white blood cells. Acute leukaemia means it progresses rapidly and aggressively, and usually requires immediate treatment."),
('myeloid leukaemia', '', "Leukaemia is cancer of the white blood cells. Acute leukaemia means it progresses rapidly and aggressively, and usually requires immediate treatment."),
('Anaphylaxis', '', "Anaphylaxis is a severe, potentially life-threatening allergic reaction that can develop rapidly."),
('Anorexia nervosa', '', "Anorexia nervosa is an eating disorder and a serious, potentially life-threatening, mental health condition."),
('Angioedema', '', "Angioedema is the swelling of the deeper layers of the skin, caused by a build-up of fluid."),
('Abdominal', '', "An abdominal aortic aneurysm (AAA) is a swelling (aneurysm) of the aorta – the main blood vessel that leads away from the heart, down through the abdomen to the rest of the body.");

CREATE TABLE ehr_visit (
  visit_id          SERIAL PRIMARY KEY NOT NULL UNIQUE,
  vi_date           DATE               NOT NULL,
  permanent_disease BOOLEAN            NOT NULL,
  vi_time           TIME               NOT NULL,
  person_id         INT                NOT NULL,
  health_centre_id  INT                NOT NULL,
  disease_id        VARCHAR(255)       NOT NULL,
  doctor_id         INT                NOT NULL,
  FOREIGN KEY (person_id) REFERENCES ehr_person (person_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (doctor_id) REFERENCES ehr_doctor (doctor_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (health_centre_id) REFERENCES ehr_health_centre (centre_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (disease_id) REFERENCES ehr_disease (english_name)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

INSERT INTO `ehr`.`ehr_visit` (`visit_id`, `vi_date`, `permanent_disease`, `vi_time`, `person_id`, `health_centre_id`, `disease_id`, `doctor_id`)
VALUES (1, '1399-03-21', 0, "23:31:34", 1, 1, 'Abdominal', "2"),
(2, '1398-04-12', 0, "08:31:34", 1, 1, 'Abdominal', "3"),
(3, '1396-02-28', 0, "09:12:14", 2, 2, 'Abdominal', "4"),
(4, '1399-03-11', 1, "10:23:24", 3, 3, 'Abdominal', "6"),
(5, '1396-04-21', 0, "12:34:34", 4, 4, 'Abdominal', "2"),
(6, '1395-05-16', 1, "11:45:14", 5, 5, 'Abdominal', "7"),
(7, '1392-03-18', 0, "14:56:44", 6, 1, 'Abdominal', "2"),
(8, '1393-06-12', 1, "16:11:54", 7, 2, 'Abdominal', "5"),
(9, '1394-07-02', 0, "17:32:54", 1, 6, 'Abdominal', "8"),
(10, '1395-12-11', 1, "18:51:34", 3, 7, 'Abdominal', "2");

CREATE TABLE ehr_drug (
  english_name VARCHAR(255) UNIQUE PRIMARY KEY NOT NULL,
  persian_name VARCHAR(255)                    ,
  descript  VARCHAR(1000)                   NOT NULL
);


INSERT INTO `ehr`.`ehr_drug` (`english_name`, `persian_name`, `descript`)
VALUES ('Amitriptyline', '', "Amitriptyline is a tricyclic antidepressant with sedative effects. Amitriptyline affects certain chemical messengers (neurotransmitters) that communicate between brain cells and help regulate mood."),
('Amlodipine', '', "Amlodipine is a calcium channel blocker that dilates (widens) blood vessels and improves blood flow."),
('Amoxicillin', '', "Amoxicillin is a penicillin antibiotic that fights bacteria."),
('Ativan', '', "Ativan (lorazepam) belongs to a group of drugs called benzodiazepines. It is thought that lorazepam works by enhancing the activity of certain neurotransmitters in the brain."),
('Atorvastatin', '', "Atorvastatin belongs to a group of drugs called HMG CoA reductase inhibitors, or statins."),
('Azithromycin', '', "Azithromycin is an antibiotic that fights bacteria."),
('Benzonatate', '', "Benzonatate is a non-narcotic cough medicine."),
('Brilinta', '', "Brilinta (ticagrelor) prevents platelets in your blood from sticking together to form an unwanted blood clot that could block an artery."),
('Loratadine', '', "Loratadine is an antihistamine that reduces the effects of natural chemical histamine in the body. Histamine can produce symptoms of sneezing, itching, watery eyes, and runny nose."),
('Lisinopril', '', "Lisinopril is an ACE inhibitor. ACE stands for angiotensin converting enzyme.");


CREATE TABLE ehr_drug_store (
  drug_store_id      INT PRIMARY KEY UNIQUE NOT NULL,
  ds_name            VARCHAR(255)           NOT NULL,
  address            VARCHAR(1000)          NOT NULL,
  zip_code           VARCHAR(10)            NOT NULL,
  phone              VARCHAR(11)            NOT NULL,
  establishment_date DATE                   NOT NULL,
  fk_head            INT                    NOT NULL,
  FOREIGN KEY (fk_head) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);


INSERT INTO `ehr`.`ehr_drug_store` (`drug_store_id`, `ds_name`, `address`, `zip_code`, `phone`, `establishment_date`, `fk_head`)
VALUES 
(1,"Ali Panahi Drug Store","Iran, Tehran","72716716" ,"1234567891",  '1399-03-21', "2"),
(2,"Dr.Rezaei Drug Store","Iran, Isfahan","72746276" ,"1234567891", '1398-04-12', "3"),
(3,"Drug Store 01","Iran, Qom", "127542716" ,"12456891",'1396-02-28',"2"),
(4,"Drug Store 02","Iran, Tabriz","143562716" , "134567891", '1399-03-11', "6"),
(5,"Drug Store 03","Iran, Tehran", "4274625326" , "13567891",'1396-04-21',"2"),
(6,"Drug Store 04","Iran, Birjand", "227232716" ,"123567891", '1395-05-16',"3"),
(7,"Drug Store 05","Iran, Mashhad", "121462123" ,"123567891", '1392-03-18',"2"),
(8,"Drug Store 06","Iran, Shiraz", "327543516" ,"123467891", '1393-06-12', "2"),
(9,"Drug Store 07" ,"Iran, Tabriz","127562716" , "123467891",'1394-07-02', "8"),
(10,"Drug Store 08","Iran, Tabriz","124362716" , "123467891", '1395-12-11',"2");

CREATE TABLE ehr_prescription (
  prescription_id SERIAL PRIMARY KEY UNIQUE NOT NULL,
  permanent       BOOLEAN                   NOT NULL,
  medicine_bought BOOLEAN                   NOT NULL,
  start_time      DATE                      NOT NULL,
  end_time        DATE                      NOT NULL,
  visit_id        BIGINT UNSIGNED                       NOT NULL,
  drug_store_id   INT                       NOT NULL,
  FOREIGN KEY (visit_id) REFERENCES ehr_visit (visit_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (drug_store_id) REFERENCES ehr_drug_store (drug_store_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);


INSERT INTO `ehr`.`ehr_perception` (`prescription_id`, `permanent`, `medicine_bought`, `start_time`, `end_time`, `visit_id`, `drug_store_id`)
VALUES 
(1,0,1,'1399-03-11','1399-04-21',1, 1),
(2,0,1,'1399-04-12','1399-05-21',2, 2),
(3,1,1,'1399-05-13','1399-06-21',3, 3),
(4,1,0,'1399-06-14','1399-07-21',4, 4),
(5,1,0,'1399-07-15','1399-08-21',5, 1),
(6,0,0,'1399-08-16','1399-09-21',6, 3),
(7,1,0,'1399-09-17','1399-10-21',7, 5),
(8,1,1,'1399-03-18','1399-06-21',8, 1),
(9,0 ,1,'1399-03-19','1399-06-21',9, 2),
(10,1,1,'1399-03-21','1399-06-21',10, 5);
CREATE TABLE ehr_drug_prescription (
  dp_id         SERIAL PRIMARY KEY NOT NULL,
  drug_id       VARCHAR(255)       NOT NULL,
  prescription_id BIGINT unsigned                NOT NULL,
  FOREIGN KEY (drug_id) REFERENCES ehr_drug (english_name)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (prescription_id) REFERENCES ehr_prescription (prescription_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

INSERT INTO `ehr`.`ehr_drug_prescription` (`dp_id`, `drug_id`, `prescription_id`)
VALUES 
(1,"Amitriptyline",1),
(2,"Lisinopril",1),
(3,"Amitriptyline",2),
(4,"Ativan",3),
(5,"Amitriptyline",4),
(6,"Lisinopril",5),
(7,"Amitriptyline",5),
(8,"Azithromycin",6),
(9,"Lisinopril",7),
(10,"Azithromycin",8);

CREATE TABLE ehr_test (
  test_id          SERIAL PRIMARY KEY UNIQUE NOT NULL,
  te_time          TIME                      NOT NULL,
  te_date          DATE                      NOT NULL,
  test_type        VARCHAR(255)              NOT NULL,
  te_result        VARCHAR(255)              NOT NULL,
  te_comment       VARCHAR(1000)             NOT NULL,
  person_id        INT                       NOT NULL,
  health_centre_id INT                       NOT NULL,
  doctor_id        INT                       NOT NULL,
  FOREIGN KEY (person_id) REFERENCES ehr_person (person_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (doctor_id) REFERENCES ehr_doctor (doctor_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (health_centre_id) REFERENCES ehr_health_centre (centre_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);



INSERT INTO `ehr`.`ehr_test` (`test_id`, `te_time`, `te_date`, `test_type`, `te_result`, `te_comment`, `person_id`, `health_centre_id`, `doctor_id`)
VALUES 
('1','14:01:34', '1399-03-11', 'Blood Test', 'O+', 'Some comment text', '1', '1', '2'),
('2','15:11:34', '1398-04-11', 'Blood Test', 'O+', 'Some comment text', '3', '1', '2'),
('3','16:21:34', '1397-05-11', 'Blood Test', 'O+', 'Some comment text', '4', '1', '2'),
('4','17:31:34', '1396-06-11', 'Blood Test', 'O+', 'Some comment text', '5', '1', '2'),
('5','18:41:34', '1395-07-11', 'Some other kind of Test01', 'Test result01', 'Some comment text', '4', '2', '3'),
('6','19:51:34', '1395-08-11', 'Some other kind of Test02', 'Test result02', 'Some comment text', '5', '2', '3'),
('7','20:21:34', '1398-09-11', 'Some other kind of Test03', 'Test result03', 'Some comment text', '6', '2', '3'),
('8','21:24:34', '1391-10-11', 'Blood Test', 'AB', 'Some comment text', '7', '3', '2'),
('9','22:37:34', '1388-11-11', 'Blood Test', 'O-', 'Some comment text', '8', '4', '5'),
('10','23:27:34', '1387-12-11', 'Blood Test', 'Test result04', 'Some comment text', '9', '5', '6');

CREATE TABLE ehr_surgery (
  surgery_id       SERIAL PRIMARY KEY UNIQUE  NOT NULL,
  su_name          VARCHAR(255)               NOT NULL,
  english_name     VARCHAR(255)               NOT NULL,
  su_date          DATE                       NOT NULL,
  surgery_type     VARCHAR(255)               NOT NULL,
  descript      VARCHAR(1000)              NOT NULL,
  su_comment       VARCHAR(1000)              NOT NULL,
  su_time          TIME                       NOT NULL,
  person_id        INT                        NOT NULL,
  doctor_id        INT                        NOT NULL,
  health_centre_id INT                        NOT NULL,
  FOREIGN KEY (person_id) REFERENCES ehr_person (person_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (health_centre_id) REFERENCES ehr_health_centre (centre_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (doctor_id) REFERENCES ehr_doctor (doctor_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
INSERT INTO `ehr`.`ehr_surgery` (`surgery_id`, `su_name`, `english_name`, `su_date`, `surgery_type`, `descript`, `su_comment`, `su_time`, `person_id`, `doctor_id`, `health_centre_id`)
VALUES 
('1','Surgery 01', 'Surgery 01 English Name','1399-01-01', 'Brain Surgery 01', 'Description ...', 'Some comment text', '04:01:34' , '1', '2', '2'),
('2','Surgery 02', 'Surgery 02 English Name','1398-02-21', 'Brain Surgery 02', 'Description ...', 'Some comment text', '09:21:34' , '2', '3', '3'),
('3','Surgery 03', 'Surgery 03 English Name','1397-03-12', 'Brain Surgery 03', 'Description ...', 'Some comment text', '14:31:34' , '3', '4', '4'),
('4','Surgery 04', 'Surgery 04 English Name','1396-04-13', 'Brain Surgery 04', 'Description ...', 'Some comment text', '15:44:34' , '4', '1', '5'),
('5','Surgery 05', 'Surgery 05 English Name','1395-05-14', 'Brain Surgery 05', 'Description ...', 'Some comment text', '16:45:34' , '5', '1', '6'),
('6','Surgery 06', 'Surgery 06 English Name','1394-06-15', 'Brain Surgery 06', 'Description ...', 'Some comment text', '17:12:34' , '6', '7', '7'),
('7','Surgery 07', 'Surgery 07 English Name','1393-07-16', 'Brain Surgery 07', 'Description ...', 'Some comment text', '18:14:34' , '7', '8', '8'),
('8','Surgery 08', 'Surgery 08 English Name','1396-08-17', 'Brain Surgery 08', 'Description ...', 'Some comment text', '19:24:34' , '8', '6', '1'),
('9','Surgery 09', 'Surgery 09 English Name','1395-09-18', 'Brain Surgery 09', 'Description ...', 'Some comment text', '20:31:34' , '8', '4', '3'),
('10','Surgery 10', 'Surgery 10 English Name','1397-10-19', 'Brain Surgery 10', 'Description ...', 'Some comment text', '14:51:34' , '9', '3', '4');


CREATE TABLE ehr_workplace (
  w_id             SERIAL PRIMARY KEY UNIQUE NOT NULL,
  doctor_id        INT                       NOT NULL,
  health_centre_id INT                       NOT NULL,
  date_from        DATE                      NOT NULL,
  date_to          DATE,
  FOREIGN KEY (health_centre_id) REFERENCES ehr_health_centre (centre_id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
  FOREIGN KEY (doctor_id) REFERENCES ehr_doctor (doctor_id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT
);


INSERT INTO `ehr`.`ehr_workplace` (`w_id`, `doctor_id`, `health_centre_id`, `date_from`, `date_to`)
VALUES 
('1','1', '1', '1399-01-01', '1399-09-01'),
('2','2', '2', '1398-02-01', null),
('3','3', '3', '1397-03-01', null),
('4','4', '1', '1396-04-01', '1398-10-01'),
('5','5', '4', '1395-05-01', '1398-11-01'),
('6','6', '6', '1394-06-01', '1397-12-01'),
('7','1', '6', '1393-07-01', '1398-12-01'),
('8','2', '3', '1392-08-01', '1398-12-01'),
('9','7', '8', '1391-09-01', '1398-12-01'),
('10','9', '9', '1390-10-01', '1398-12-01');

