CREATE TABLE ehr_doctor (
  doctor_id       INT PRIMARY KEY UNIQUE          NOT NULL,
  personal_digit  VARCHAR(12) UNIQUE                      NOT NULL,
  do_degree       VARCHAR(255)                    NOT NULL,
  major           VARCHAR(255)                    NOT NULL,
  graduated_from  VARCHAR(255)                    NOT NULL,
  number_of_visit INT,
  salary          TEXT                             NOT NULL,
  FOREIGN KEY (doctor_id) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

INSERT INTO `ehr`.`ehr_doctor` (`doctor_id`, `personal_digit`, `do_degree`, `major`, `graduated_from`, `number_of_visit`, `salary`)
VALUES ('1', '965412134587', 'PhD', 'Allergists' ,'University of Tehran', 54 , '12000000'),
('2', '975487134587', 'PhD', 'Allergists' ,'University of Tehran', 12 , '12000000'),
('3', '925423114587', 'PhD', 'Dermatologists' ,'University of Tehran', 524 , '12000000'),
('4', '955117234587', 'PhD', 'Ophthalmologists' ,'University of Tehran', 514 , '12000000'),
('5', '931117134587', 'PhD', 'Obstetrician' ,'University of Tehran', 514 , '12000000'),
('6', '965482224587', 'PhD', 'Cardiologists' ,'Shahid Beheshti University', 154 , '12000000'),
('7', '915487133337', 'PhD', 'Obstetrician' ,'University of Tehran', 243 , '12000000'),
('8', '955487124587', 'PhD', 'Dentist' ,'University of Iran', 321 , '12000000'),
('9', '975443134587', 'PhD', 'Cardiologists' ,'Shahid Beheshti University', 114 , '12000000');