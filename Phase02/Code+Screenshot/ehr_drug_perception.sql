CREATE TABLE ehr_drug_prescription (
  dp_id         SERIAL PRIMARY KEY NOT NULL,
  drug_id       VARCHAR(255)       NOT NULL,
  prescription_id BIGINT unsigned                NOT NULL,
  FOREIGN KEY (drug_id) REFERENCES ehr_drug (english_name)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (prescription_id) REFERENCES ehr_prescription (prescription_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

INSERT INTO `ehr`.`ehr_drug_prescription` (`dp_id`, `drug_id`, `prescription_id`)
VALUES 
(1,"Amitriptyline",1),
(2,"Lisinopril",1),
(3,"Amitriptyline",2),
(4,"Ativan",3),
(5,"Amitriptyline",4),
(6,"Lisinopril",5),
(7,"Amitriptyline",5),
(8,"Azithromycin",6),
(9,"Lisinopril",7),
(10,"Azithromycin",8);