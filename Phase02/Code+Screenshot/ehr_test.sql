CREATE TABLE ehr_test (
  test_id          SERIAL PRIMARY KEY UNIQUE NOT NULL,
  te_time          TIME                      NOT NULL,
  te_date          DATE                      NOT NULL,
  test_type        VARCHAR(255)              NOT NULL,
  te_result        VARCHAR(255)              NOT NULL,
  te_comment       VARCHAR(1000)             NOT NULL,
  person_id        INT                       NOT NULL,
  health_centre_id INT                       NOT NULL,
  doctor_id        INT                       NOT NULL,
  FOREIGN KEY (person_id) REFERENCES ehr_person (person_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (doctor_id) REFERENCES ehr_doctor (doctor_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (health_centre_id) REFERENCES ehr_health_centre (centre_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

INSERT INTO `ehr`.`ehr_test` (`test_id`, `te_time`, `te_date`, `test_type`, `te_result`, `te_comment`, `person_id`, `health_centre_id`, `doctor_id`)
VALUES 
('1','14:01:34', '1399-03-11', 'Blood Test', 'O+', 'Some comment text', '1', '1', '2'),
('2','15:11:34', '1398-04-11', 'Blood Test', 'O+', 'Some comment text', '3', '1', '2'),
('3','16:21:34', '1397-05-11', 'Blood Test', 'O+', 'Some comment text', '4', '1', '2'),
('4','17:31:34', '1396-06-11', 'Blood Test', 'O+', 'Some comment text', '5', '1', '2'),
('5','18:41:34', '1395-07-11', 'Some other kind of Test01', 'Test result01', 'Some comment text', '4', '2', '3'),
('6','19:51:34', '1395-08-11', 'Some other kind of Test02', 'Test result02', 'Some comment text', '5', '2', '3'),
('7','20:21:34', '1398-09-11', 'Some other kind of Test03', 'Test result03', 'Some comment text', '6', '2', '3'),
('8','21:24:34', '1391-10-11', 'Blood Test', 'AB', 'Some comment text', '7', '3', '2'),
('9','22:37:34', '1388-11-11', 'Blood Test', 'O-', 'Some comment text', '8', '4', '5'),
('10','23:27:34', '1387-12-11', 'Blood Test', 'Test result04', 'Some comment text', '9', '5', '6');