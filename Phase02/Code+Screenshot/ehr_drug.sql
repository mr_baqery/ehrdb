CREATE TABLE ehr_drug (
  english_name VARCHAR(255) UNIQUE PRIMARY KEY NOT NULL,
  persian_name VARCHAR(255)                    ,
  descript  VARCHAR(1000)                   NOT NULL
);


INSERT INTO `ehr`.`ehr_drug` (`english_name`, `persian_name`, `descript`)
VALUES ('Amitriptyline', '', "Amitriptyline is a tricyclic antidepressant with sedative effects. Amitriptyline affects certain chemical messengers (neurotransmitters) that communicate between brain cells and help regulate mood."),
('Amlodipine', '', "Amlodipine is a calcium channel blocker that dilates (widens) blood vessels and improves blood flow."),
('Amoxicillin', '', "Amoxicillin is a penicillin antibiotic that fights bacteria."),
('Ativan', '', "Ativan (lorazepam) belongs to a group of drugs called benzodiazepines. It is thought that lorazepam works by enhancing the activity of certain neurotransmitters in the brain."),
('Atorvastatin', '', "Atorvastatin belongs to a group of drugs called HMG CoA reductase inhibitors, or statins."),
('Azithromycin', '', "Azithromycin is an antibiotic that fights bacteria."),
('Benzonatate', '', "Benzonatate is a non-narcotic cough medicine."),
('Brilinta', '', "Brilinta (ticagrelor) prevents platelets in your blood from sticking together to form an unwanted blood clot that could block an artery."),
('Loratadine', '', "Loratadine is an antihistamine that reduces the effects of natural chemical histamine in the body. Histamine can produce symptoms of sneezing, itching, watery eyes, and runny nose."),
('Lisinopril', '', "Lisinopril is an ACE inhibitor. ACE stands for angiotensin converting enzyme.");
