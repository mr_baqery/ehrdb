CREATE TABLE ehr_drug_store (
  drug_store_id      INT PRIMARY KEY UNIQUE NOT NULL,
  ds_name            VARCHAR(255)           NOT NULL,
  address            VARCHAR(1000)          NOT NULL,
  zip_code           VARCHAR(10)            NOT NULL,
  phone              VARCHAR(11)            NOT NULL,
  establishment_date DATE                   NOT NULL,
  fk_head            INT                    NOT NULL,
  FOREIGN KEY (fk_head) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);


INSERT INTO `ehr`.`ehr_drug_store` (`drug_store_id`, `ds_name`, `address`, `zip_code`, `phone`, `establishment_date`, `fk_head`)
VALUES 
(1,"Ali Panahi Drug Store","Iran, Tehran","72716716" ,"1234567891",  '1399-03-21', "2"),
(2,"Dr.Rezaei Drug Store","Iran, Isfahan","72746276" ,"1234567891", '1398-04-12', "3"),
(3,"Drug Store 01","Iran, Qom", "127542716" ,"12456891",'1396-02-28',"2"),
(4,"Drug Store 02","Iran, Tabriz","143562716" , "134567891", '1399-03-11', "6"),
(5,"Drug Store 03","Iran, Tehran", "4274625326" , "13567891",'1396-04-21',"2"),
(6,"Drug Store 04","Iran, Birjand", "227232716" ,"123567891", '1395-05-16',"3"),
(7,"Drug Store 05","Iran, Mashhad", "121462123" ,"123567891", '1392-03-18',"2"),
(8,"Drug Store 06","Iran, Shiraz", "327543516" ,"123467891", '1393-06-12', "2"),
(9,"Drug Store 07" ,"Iran, Tabriz","127562716" , "123467891",'1394-07-02', "8"),
(10,"Drug Store 08","Iran, Tabriz","124362716" , "123467891", '1395-12-11',"2");