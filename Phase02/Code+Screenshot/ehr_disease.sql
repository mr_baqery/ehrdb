
CREATE TABLE ehr_disease (
  english_name VARCHAR(255) UNIQUE PRIMARY KEY NOT NULL,
  persian_name VARCHAR(255)                    ,
  descript  VARCHAR(1000)                   NOT NULL
);

INSERT INTO `ehr`.`ehr_disease` (`english_name`, `persian_name`, `descript`)
VALUES ('Abdominal aortic aneurysm', '', "An abdominal aortic aneurysm (AAA) is a swelling (aneurysm) of the aorta – the main blood vessel that leads away from the heart, down through the abdomen to the rest of the body."),
('Acne', '', "Acne is a common skin condition that affects most people at some point. It causes spots, oily skin and sometimes skin that's hot or painful to touch."),
('Acute cholecystitis', '', "Acute cholecystitis is swelling (inflammation) of the gallbladder. It is a potentially serious condition that usually needs to be treated in hospital."),
('leukaemia', '', "This section is for teenagers and young adults and is about a type of cancer called acute lymphoblastic leukaemia (ALL). The other main type of leukaemia that can affect teenagers and young adults is acute myeloid leukaemia."),
('Acute lymphoblastic leukaemia', '', "Leukaemia is cancer of the white blood cells. Acute leukaemia means it progresses rapidly and aggressively, and usually requires immediate treatment."),
('myeloid leukaemia', '', "Leukaemia is cancer of the white blood cells. Acute leukaemia means it progresses rapidly and aggressively, and usually requires immediate treatment."),
('Anaphylaxis', '', "Anaphylaxis is a severe, potentially life-threatening allergic reaction that can develop rapidly."),
('Anorexia nervosa', '', "Anorexia nervosa is an eating disorder and a serious, potentially life-threatening, mental health condition."),
('Angioedema', '', "Angioedema is the swelling of the deeper layers of the skin, caused by a build-up of fluid."),
('Abdominal', '', "An abdominal aortic aneurysm (AAA) is a swelling (aneurysm) of the aorta – the main blood vessel that leads away from the heart, down through the abdomen to the rest of the body.");