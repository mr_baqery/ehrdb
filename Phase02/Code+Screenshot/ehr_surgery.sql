CREATE TABLE ehr_surgery (
  surgery_id       SERIAL PRIMARY KEY UNIQUE  NOT NULL,
  su_name          VARCHAR(255)               NOT NULL,
  english_name     VARCHAR(255)               NOT NULL,
  su_date          DATE                       NOT NULL,
  surgery_type     VARCHAR(255)               NOT NULL,
  descript      VARCHAR(1000)              NOT NULL,
  su_comment       VARCHAR(1000)              NOT NULL,
  su_time          TIME                       NOT NULL,
  person_id        INT                        NOT NULL,
  doctor_id        INT                        NOT NULL,
  health_centre_id INT                        NOT NULL,
  FOREIGN KEY (person_id) REFERENCES ehr_person (person_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (health_centre_id) REFERENCES ehr_health_centre (centre_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (doctor_id) REFERENCES ehr_doctor (doctor_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
INSERT INTO `ehr`.`ehr_surgery` (`surgery_id`, `su_name`, `english_name`, `su_date`, `surgery_type`, `descript`, `su_comment`, `su_time`, `person_id`, `doctor_id`, `health_centre_id`)
VALUES 
('1','Surgery 01', 'Surgery 01 English Name','1399-01-01', 'Brain Surgery 01', 'Description ...', 'Some comment text', '04:01:34' , '1', '2', '2'),
('2','Surgery 02', 'Surgery 02 English Name','1398-02-21', 'Brain Surgery 02', 'Description ...', 'Some comment text', '09:21:34' , '2', '3', '3'),
('3','Surgery 03', 'Surgery 03 English Name','1397-03-12', 'Brain Surgery 03', 'Description ...', 'Some comment text', '14:31:34' , '3', '4', '4'),
('4','Surgery 04', 'Surgery 04 English Name','1396-04-13', 'Brain Surgery 04', 'Description ...', 'Some comment text', '15:44:34' , '4', '1', '5'),
('5','Surgery 05', 'Surgery 05 English Name','1395-05-14', 'Brain Surgery 05', 'Description ...', 'Some comment text', '16:45:34' , '5', '1', '6'),
('6','Surgery 06', 'Surgery 06 English Name','1394-06-15', 'Brain Surgery 06', 'Description ...', 'Some comment text', '17:12:34' , '6', '7', '7'),
('7','Surgery 07', 'Surgery 07 English Name','1393-07-16', 'Brain Surgery 07', 'Description ...', 'Some comment text', '18:14:34' , '7', '8', '8'),
('8','Surgery 08', 'Surgery 08 English Name','1396-08-17', 'Brain Surgery 08', 'Description ...', 'Some comment text', '19:24:34' , '8', '6', '1'),
('9','Surgery 09', 'Surgery 09 English Name','1395-09-18', 'Brain Surgery 09', 'Description ...', 'Some comment text', '20:31:34' , '8', '4', '3'),
('10','Surgery 10', 'Surgery 10 English Name','1397-10-19', 'Brain Surgery 10', 'Description ...', 'Some comment text', '14:51:34' , '9', '3', '4');