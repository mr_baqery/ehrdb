
CREATE TABLE ehr_health_centre (
  centre_id          INT PRIMARY KEY NOT NULL  UNIQUE,
  hc_name            VARCHAR(255)    NOT NULL,
  address            VARCHAR(1000)   NOT NULL,
  zip_code           VARCHAR(10)             NOT NULL,
  private            BOOLEAN         NOT NULL,
  phone              VARCHAR(11)             NOT NULL,
  establishment_date DATE            NOT NULL,
  fk_head            INT             NOT NULL,
  FOREIGN KEY (fk_head) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

INSERT INTO `ehr`.`ehr_health_centre` (`centre_id`, `hc_name`, `address`, `zip_code`, `private`, `phone`, `establishment_date`, `fk_head`)
VALUES ('1', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41526359874', '1366-03-21', '1'),
('2', 'Shahid Beheshti', 'Irean , Tehran', '1234567891', '1', '41526359874', '1366-12-11', '1'),
('3', 'Tehran', 'Irean , Tehran', '1234567891', '1', '43526359874', '1376-03-21', '2'),
('4', 'Masih Daneshavri', 'Irean , Tehran', '1234567891', '1', '41526359874', '1386-11-23', '2'),
('5', 'Iran', 'Irean , Tehran', '1234567891', '1', '41526326824', '1396-09-24', '1'),
('6', 'Health Centre 01', 'Irean , Tehran', '1234567891', '1', '41526359874', '1336-05-12', '1'),
('7', 'Health Centre 02', 'Irean , Tehran', '1234567891', '1', '35926359874', '1346-02-25', '3'),
('8', 'Health Centre 03', 'Irean , Tehran', '1234567891', '1', '41526359874', '1356-04-21', '3'),
('9', 'Health Centre 04', 'Irean , Tehran', '1234567891', '1', '98535959874', '1366-01-21', '3'),
('10', 'Health Centre 05', 'Irean , Tehran', '1234567891', '1', '41524159414', '1366-03-21','1');
