CREATE TABLE ehr_prescription (
  prescription_id SERIAL PRIMARY KEY UNIQUE NOT NULL,
  permanent       BOOLEAN                   NOT NULL,
  medicine_bought BOOLEAN                   NOT NULL,
  start_time      DATE                      NOT NULL,
  end_time        DATE                      NOT NULL,
  visit_id        BIGINT UNSIGNED                       NOT NULL,
  drug_store_id   INT                       NOT NULL,
  FOREIGN KEY (visit_id) REFERENCES ehr_visit (visit_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (drug_store_id) REFERENCES ehr_drug_store (drug_store_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

INSERT INTO `ehr`.`ehr_prescription` (`prescription_id`, `permanent`, `medicine_bought`, `start_time`, `end_time`, `visit_id`, `drug_store_id`)
VALUES 
(1,0,1,'1399-03-11','1399-04-21',1, 1),
(2,0,1,'1399-04-12','1399-05-21',2, 2),
(3,1,1,'1399-05-13','1399-06-21',3, 3),
(4,1,0,'1399-06-14','1399-07-21',4, 4),
(5,1,0,'1399-07-15','1399-08-21',5, 1),
(6,0,0,'1399-08-16','1399-09-21',6, 3),
(7,1,0,'1399-09-17','1399-10-21',7, 5),
(8,1,1,'1399-03-18','1399-06-21',8, 1),
(9,0 ,1,'1399-03-19','1399-06-21',9, 2),
(10,1,1,'1399-03-21','1399-06-21',10, 5);