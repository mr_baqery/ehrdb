CREATE TABLE ehr_person (
  person_id           INT PRIMARY KEY UNIQUE NOT NULL,
  first_name          VARCHAR(255)           NOT NULL,
  last_name           VARCHAR(255)           NOT NULL,
  age                 INT                    NOT NULL,
  birth_day           DATE                   NOT NULL,
  alive               BOOLEAN                NOT NULL,
  birth_place         VARCHAR(255)           NOT NULL,
  phone_number        VARCHAR(11)            NOT NULL,
  mobile_phone_number VARCHAR(11)            NOT NULL,
  allergies           VARCHAR(1000),
  residence_address   VARCHAR(1000)          NOT NULL,
  gender              VARCHAR(100)           NOT NULL,
  mother              INT,
  father              INT,
  FOREIGN KEY (mother) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (father) REFERENCES ehr_person (person_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- Insert data
INSERT INTO `ehr`.`ehr_person` (`person_id`, `first_name`, `last_name`, `age`, `birth_day`, `alive`, `birth_place`, `phone_number`, `mobile_phone_number`, `residence_address`, `gender`)
VALUES ('1', 'Reza', 'Akbari', '23', '1356-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'male'),
('2', 'Ali', 'Sohrabi', '33', '1361-03-04', '1', 'Isfahan', '64851794', '19159462693', 'Iran, Isfahan', 'male'),
('3', 'Akbar', 'Rezaie', '43', '1384-03-04', '1', 'Shiraz', '64851794', '19159462693', 'Iran, Shiraz', 'male'),
('4', 'Sohrab', 'Mahmodi', '53', '1379-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'male'),
('5', 'Jimmy', 'Abbasi', '13', '1369-03-04', '1', 'Mashad', '64851794', '19159462693', 'Iran, Mashad', 'male'),
('6', 'Mahmod', 'Ahmadi', '21', '1364-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'male'),
('7', 'Abbas', 'Akbari', '29', '1362-03-04', '1', 'Qom', '64851794', '19159462693', 'Iran, Qom', 'male'),
('8', 'Zahra', 'Sohrabi', '24', '1396-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'female'),
('9', 'Zeinab', 'Ahmadi', '45', '1386-03-04', '1', 'Tabriz', '64851794', '19159462693', 'Iran, Tabriz', 'female'),
('10', 'Sakine', 'Namazi', '94', '1376-03-04', '1', 'Tehran', '64851794', '19159462693', 'Iran, Tehran', 'female');
