CREATE TABLE ehr_visit (
  visit_id          SERIAL PRIMARY KEY NOT NULL UNIQUE,
  vi_date           DATE               NOT NULL,
  permanent_disease BOOLEAN            NOT NULL,
  vi_time           TIME               NOT NULL,
  person_id         INT                NOT NULL,
  health_centre_id  INT                NOT NULL,
  disease_id        VARCHAR(255)       NOT NULL,
  doctor_id         INT                NOT NULL,
  FOREIGN KEY (person_id) REFERENCES ehr_person (person_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (doctor_id) REFERENCES ehr_doctor (doctor_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (health_centre_id) REFERENCES ehr_health_centre (centre_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  FOREIGN KEY (disease_id) REFERENCES ehr_disease (english_name)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

INSERT INTO `ehr`.`ehr_visit` (`visit_id`, `vi_date`, `permanent_disease`, `vi_time`, `person_id`, `health_centre_id`, `disease_id`, `doctor_id`)
VALUES (1, '1399-03-21', 0, "23:31:34", 1, 1, 'Abdominal', "2"),
(2, '1398-04-12', 0, "08:31:34", 1, 1, 'Abdominal', "3"),
(3, '1396-02-28', 0, "09:12:14", 2, 2, 'Abdominal', "4"),
(4, '1399-03-11', 1, "10:23:24", 3, 3, 'Abdominal', "6"),
(5, '1396-04-21', 0, "12:34:34", 4, 4, 'Abdominal', "2"),
(6, '1395-05-16', 1, "11:45:14", 5, 5, 'Abdominal', "7"),
(7, '1392-03-18', 0, "14:56:44", 6, 1, 'Abdominal', "2"),
(8, '1393-06-12', 1, "16:11:54", 7, 2, 'Abdominal', "5"),
(9, '1394-07-02', 0, "17:32:54", 1, 6, 'Abdominal', "8"),
(10, '1395-12-11', 1, "18:51:34", 3, 7, 'Abdominal', "2");