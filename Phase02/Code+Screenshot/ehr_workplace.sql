CREATE TABLE ehr_workplace (
  w_id             SERIAL PRIMARY KEY UNIQUE NOT NULL,
  doctor_id        INT                       NOT NULL,
  health_centre_id INT                       NOT NULL,
  date_from        DATE                      NOT NULL,
  date_to          DATE,
  FOREIGN KEY (health_centre_id) REFERENCES ehr_health_centre (centre_id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
  FOREIGN KEY (doctor_id) REFERENCES ehr_doctor (doctor_id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT
);

INSERT INTO `ehr`.`ehr_workplace` (`w_id`, `doctor_id`, `health_centre_id`, `date_from`, `date_to`)
VALUES 
('1','1', '1', '1399-01-01', '1399-09-01'),
('2','2', '2', '1398-02-01', null),
('3','3', '3', '1397-03-01', null),
('4','4', '1', '1396-04-01', '1398-10-01'),
('5','5', '4', '1395-05-01', '1398-11-01'),
('6','6', '6', '1394-06-01', '1397-12-01'),
('7','1', '6', '1393-07-01', '1398-12-01'),
('8','2', '3', '1392-08-01', '1398-12-01'),
('9','7', '8', '1391-09-01', '1398-12-01'),
('10','9', '9', '1390-10-01', '1398-12-01');
