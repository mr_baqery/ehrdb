SELECT 
    doctor_id,
    health_centre_id,
    date_from,
    date_to,
    w_id,
    centre_id,
    hc_name,
    address,
    zip_code,
    private,
    phone,
    establishment_date,
    fk_head
FROM
    ehr_health_centre
        INNER JOIN
    ehr_workplace ON ehr_health_centre.centre_id = ehr_workplace.health_centre_id
WHERE
    centre_id = 1