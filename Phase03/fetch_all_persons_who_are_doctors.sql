SELECT 
    *
FROM
    ehr_person
        INNER JOIN
    ehr_doctor ON ehr_person.person_id = ehr_doctor.doctor_id
