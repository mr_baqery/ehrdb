SELECT 
    english_name,
    persian_name,
    descript,
    permanent_disease,
    person_id,
    health_centre_id,
    disease_id,
    doctor_id,
    vi_time,
    vi_date,
    visit_id
FROM
    ehr_visit
        INNER JOIN
    ehr_disease ON ehr_visit.disease_id = ehr_disease.english_name
WHERE
    visit_id = 1