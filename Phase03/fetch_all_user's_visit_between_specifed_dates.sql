SELECT 
    permanent,
    medicine_bought,
    start_time,
    end_time,
    ehr_visit.visit_id,
    vi_date,
    vi_time,
    drug_id
FROM
    ehr_prescription
        INNER JOIN
    ehr_visit ON ehr_prescription.visit_id = ehr_visit.visit_id
        INNER JOIN
    ehr_drug_prescription ON ehr_drug_prescription.prescription_id = ehr_prescription.prescription_id
WHERE
    DATE(start_time) > DATE('1377-03-12')
        AND DATE(end_time) < DATE('1399-09-12')
        AND ehr_visit.visit_id = 1