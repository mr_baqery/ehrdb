SELECT 
    english_name,
    persian_name,
    descript,
    permanent_disease,
    person_id,
    health_centre_id,
    disease_id,
    doctor_id,
    vi_time,
    vi_date,
    visit_id
FROM
    ehr_disease
        JOIN
    ehr_visit ON ehr_disease.english_name = ehr_visit.disease_id
WHERE
    permanent_disease = 1
        AND person_id = 3