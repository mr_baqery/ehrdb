SELECT 
    permanent,
    medicine_bought,
    start_time,
    end_time,
    ehr_visit.visit_id,
    vi_date,
    vi_time,
    drug_id
FROM
    ehr_prescription
        INNER JOIN
    ehr_visit ON ehr_prescription.visit_id = ehr_visit.visit_id
        INNER JOIN
    ehr_drug_prescription ON ehr_drug_prescription.prescription_id = ehr_prescription.prescription_id
WHERE
    medicine_bought = 1
        AND ehr_visit.visit_id = 1