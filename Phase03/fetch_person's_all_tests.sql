SELECT 
	ehr_person.person_id,
    first_name,
    last_name,
    age,
    birth_day,
    alive,
    test_type,
    te_result,
    te_comment,
    health_centre_id,
    doctor_id
FROM
    ehr_person
        INNER JOIN
    ehr_test ON ehr_person.person_id = ehr_test.person_id
where 
ehr_test.person_id = 1
