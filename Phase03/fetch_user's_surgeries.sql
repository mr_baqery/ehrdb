SELECT 
    ehr_person.person_id,
    first_name,
    last_name,
    age,
    birth_day,
    alive,
    su_name,
    english_name,
    surgery_type,
    descript,
    su_comment
FROM
    ehr_person
        INNER JOIN
    ehr_surgery ON ehr_person.person_id = ehr_surgery.person_id
