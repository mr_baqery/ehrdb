SELECT 
    english_name,
    persian_name,
    descript,
    permanent_disease,
    person_id,
    health_centre_id,
    disease_id,
    doctor_id,
    vi_time,
    vi_date,
    visit_id
FROM
    ehr_disease
        JOIN
    ehr_visit ON ehr_disease.english_name = ehr_visit.disease_id
WHERE
    DATE(vi_date) > DATE('1377-03-21')
        AND DATE(vi_date) < DATE('1399-09-21')
        AND person_id = 1