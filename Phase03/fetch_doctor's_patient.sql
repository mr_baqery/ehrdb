SELECT DISTINCT
    (ehr_person.person_id),
    first_name,
    last_name,
    age,
    birth_day,
    alive,
    birth_place,
    phone_number,
    mobile_phone_number,
    residence_address,
    gender,
    allergies,
    mother,
    father
FROM
    ehr_visit
        INNER JOIN
    ehr_person ON ehr_visit.person_id = ehr_person.person_id
WHERE
    doctor_id = 2