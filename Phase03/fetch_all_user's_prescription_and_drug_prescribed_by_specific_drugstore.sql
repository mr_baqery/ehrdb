SELECT 
    ehr_prescription.prescription_id,
    ehr_drug_prescription.drug_id,
    ehr_drug_store.address,
    ehr_drug_store.ds_name
FROM
    ehr_prescription
        JOIN
    ehr_drug_store ON ehr_prescription.drug_store_id = ehr_drug_store.drug_store_id
        JOIN
    ehr_visit ON ehr_prescription.visit_id = ehr_visit.visit_id
        JOIN
    ehr_drug_prescription ON ehr_prescription.prescription_id = ehr_drug_prescription.prescription_id
WHERE
    ehr_visit.person_id = 1
        AND ehr_drug_store.address = 'Iran, Tehran'
